package Services;

import org.testng.annotations.DataProvider;

import java.io.IOException;

/**
 * Created by igorp on 6/07/17.
 */
public class DataProviders {

    @DataProvider(name = "validExpressionParseCalc")
    public static Object[][] validExpressionParseCalc() throws IOException {
        return new Object[][] {
                {"5+5-5", "5"},
                {"5+5*5", "30"},
                {"5+5/5", "6"},
                {"5+5-5*5", "-15"},
                {"(5+5)*5", "50"},
                {"(5+5)/5", "2"},
                {"10-100", "-90"},
                {"5.5-5.5", "0"},
                {"5,5-5", "0.5"},
                {"1/10", "0.1"}
        };
    }

    @DataProvider(name = "invalidExpressionParseCalc")
    public static Object[][] invalidExpressionParseCalc() throws IOException {
        return new Object[][] {
                {"aesgagag"},
                {"5\5"},
                {"text text"},
                {"text + text"},
                {"кириллица рулит"}
        };
    }

    @DataProvider(name = "validExpressionSimpleCalc")
    public static Object[][] validExpressionSimpleCalc() throws IOException {
        return new Object[][] {
                {"5", "+", "5", "10"},
                {"5", "-", "5", "0"},
                {"5", "*", "5", "25"},
                {"5", "/", "5", "1"},
                {"5", "mod", "5", "0"},
                {"5.5", "+", "5", "10.5"},
                {"5,5", "+", "5", "10.5"},
                {"5", "+", "5.5", "10.5"},
                {"5", "+", "5,5", "10.5"},
                {"0", "+", "5", "10"},
        };
    }

    @DataProvider(name = "invalidExpressionSimpleCalc")
    public static Object[][] invalidExpressionSimpleCalc() throws IOException {
        return new Object[][] {
                {"af", "+", "5"},
                {"5", "+", "af"},
                {"5", "af", "5"},
                {"", "+", "5"},
                {"5", "+", ""},
                {"5", "", "5"},
        };
    }
}
