package Services;

import org.apache.commons.cli.Option;

/**
 * Created by igorp on 6/07/17.
 */
public class Options {

    public static org.apache.commons.cli.Options options(){

        //option parameters
        Option help = new Option("h", "help", false, "Help");
        Option firstVariant = new Option("1", false, "First simple variant");
        Option secondVariant = new Option("2", false, "Second variant, with line parser");

        org.apache.commons.cli.Options options = new org.apache.commons.cli.Options();
        options.addOption(help);
        options.addOption(firstVariant);
        options.addOption(secondVariant);

        return options;
    }
}
