import org.apache.commons.cli.Options;

import java.util.Scanner;

/**
 * Created by igorp on 6/07/17.
 */
public class InsertDataMethods {

    public static void insertDataAndCalcSimple(Options options){
        for(;;){
            Scanner scanner = new Scanner(System.in);

            //get first number from inserted line
            System.out.println("Insert first number or insert exit to finish: ");
            String firstLine = scanner.nextLine();

            if(firstLine.equals("exit"))
                break;

            //get arithmetic symbol from inserted line
            System.out.print("Insert arithmetic symbol: +, - , /, *, mod: ");
            String arithmeticSymbol = scanner.nextLine();

            //get second number from inserted line
            System.out.print("Insert second number: ");
            String secondLine = scanner.nextLine();

            String answer =  CalculateSimple.calculateNumbers(options, firstLine, arithmeticSymbol, secondLine);

            if(answer!=null){
                System.out.println("Answer is: " + answer);
                System.out.println();
            }
        }
    }

    public static void insertDataAndCalcParser(Options options){
        for(;;){

            Scanner scanner = new Scanner(System.in);

            System.out.println("Insert arithmetic expression, for example 5+5*(4/2)-7+1 , or insert exit to finish: ");

            String line = scanner.nextLine();

            if(line.equals("exit"))
                break;

            String answer = CalculateParser.calculateNumbers(options, line);

            System.out.println("Answer is: " + answer);
            System.out.println();
        }
    }
}
