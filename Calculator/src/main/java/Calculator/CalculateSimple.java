import org.apache.commons.cli.Options;

import static Services.Help.printHelpOptions;

/**
 * Created by igorp on 3/07/17.
 */
public class CalculateSimple {
    static String number;

    public static String calculateNumbers(Options options, String firstNumber, String symbol, String secondNumber){

            try {
                double firstNumberDouble = Double.parseDouble(firstNumber.replace(",", "."));
                double secondNumberDouble = Double.parseDouble(secondNumber.replace(",", "."));

                //choose arithmetic symbol
                switch (symbol) {
                    case "+":
                        number = Double.toString(firstNumberDouble + secondNumberDouble);
                        break;
                    case "-":
                        number = Double.toString(firstNumberDouble - secondNumberDouble);
                        break;
                    case "/":
                        number = Double.toString(firstNumberDouble / secondNumberDouble);
                        break;
                    case "*":
                        number = Double.toString(firstNumberDouble * secondNumberDouble);
                        break;
                    case "mod":
                        number = Double.toString(firstNumberDouble % secondNumberDouble);
                        break;
                    default:
                        //catch that symbol is incorrect, show help
                        System.out.println();
                        System.out.println("Incorrect arithmetic symbol, please insert only: +, -, /, *, mod");
                        printHelpOptions(options, 200, "Options", "-- HELP --", 3, 5, true, System.out);
                        System.out.println();
                        number = null;
                }
            }
            catch (NumberFormatException e){
                //catch exception, show help
                System.out.println();
                System.out.println("Please, insert number, for example 5, 5.25");
                printHelpOptions(options, 200, "Options", "-- HELP --", 3, 5, true, System.out);
                System.out.println();
            }
        return number;
    }
}

