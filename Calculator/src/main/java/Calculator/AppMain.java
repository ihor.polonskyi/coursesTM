import Services.*;
import org.apache.commons.cli.*;
import org.apache.commons.cli.Options;

import static Services.Help.printHelpOptions;

/**
 * Created by igorp on 3/07/17.
 */
public class AppMain {

    public static void main(String[] args) {

        Options options = Services.Options.options();

        try {
            //parse command line parameters
            CommandLineParser cmdLinePosixParser = new PosixParser();
            CommandLine commandLine = cmdLinePosixParser.parse(options, args);

            //help
            if (commandLine.hasOption("h"))
                printHelpOptions(options, 200, "Options", "-- HELP --", 3, 5, true, System.out);

            //first variant (simple)
            if (commandLine.hasOption("1"))
              InsertDataMethods.insertDataAndCalcSimple(options);

            //second variant (parser)
            if (commandLine.hasOption("2"))
              InsertDataMethods.insertDataAndCalcParser(options);

            //show help
            else {
                printHelpOptions(options, 200, "Options", "-- HELP --", 3, 5, true, System.out);
            }
        } catch (ParseException e) {
            //catch exception, show help
            Log.error("Something went wrong, see help to use program right");
            printHelpOptions(options, 200, "Options", "-- HELP --", 3, 5, true, System.out);
        }


    }
}


