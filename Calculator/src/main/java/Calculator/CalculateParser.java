import org.apache.commons.cli.Options;

import javax.script.ScriptException;

import static Services.Help.printHelpOptions;

/**
 * Created by igorp on 3/07/17.
 */
public class CalculateParser {

    static String answer;
    public static String calculateNumbers(Options options, String line){

            try {
                //use magic to calc expression
                javax.script.ScriptEngineManager mgr = new javax.script.ScriptEngineManager();
                javax.script.ScriptEngine engine = mgr.getEngineByName("JavaScript");

                answer = engine.eval(line.replace(",", ".")).toString();
            }
            catch (ScriptException e){
                //catch exception, show help
                System.out.println("Something went wrong, try again. Please, insert correct numbers, for example 5+5*(4/2)-7+1");
                printHelpOptions(options, 200, "Options", "-- HELP --", 3, 5, true, System.out);
                System.out.println();
            }
        return answer;
    }
}
