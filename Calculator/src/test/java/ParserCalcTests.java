import Services.DataProviders;
import org.apache.commons.cli.Options;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.script.ScriptException;

/**
 * Created by igorp on 6/07/17.
 */
public class ParserCalcTests {

    static Options options = Services.Options.options();

    @Test(dataProvider = "validExpressionParseCalc", dataProviderClass = DataProviders.class)
    public void test_001_positive_cases(String expression, String answer){
        Assert.assertEquals(CalculateParser.calculateNumbers(options, expression), answer,
                "Incorrect answer: "+answer+ " ,For expression: "+expression);
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "invalidExpressionParseCalc",
            expectedExceptions = ScriptException.class)
    public void test_002_negative_cases(String expression){
        CalculateParser.calculateNumbers(options, expression);
    }
}

