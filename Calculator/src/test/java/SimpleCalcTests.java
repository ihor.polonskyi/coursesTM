import Services.DataProviders;
import org.apache.commons.cli.Options;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.script.ScriptException;

/**
 * Created by igorp on 6/07/17.
 */
public class SimpleCalcTests {

    static Options options = Services.Options.options();

    @Test(dataProvider = "validExpressionSimpleCalc", dataProviderClass = DataProviders.class)
    public void test_001_positive_cases(String firstNumber, String argument, String secondNumber, String answer){
        Assert.assertEquals(CalculateSimple.calculateNumbers(options, firstNumber, argument, secondNumber), answer,
                "Incorrect answer: "+answer);
    }

    @Test(dataProviderClass = DataProviders.class, dataProvider = "invalidExpressionSimpleCalc",
            expectedExceptions = ScriptException.class)
    public void test_002_negative_cases(String firstNumber, String argument, String secondNumber){
        CalculateSimple.calculateNumbers(options, firstNumber, argument, secondNumber);
    }
}

