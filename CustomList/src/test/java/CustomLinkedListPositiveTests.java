import CustomLinkedList.CustomLinkedList;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by igorp on 6/07/17.
 */
public class CustomLinkedListPositiveTests extends BaseTest {


    @Test
    public void addToNewTest(){
        CustomLinkedList list = new CustomLinkedList();

        list.add("one");

        list.add("two");

        Assert.assertEquals(list.size(), 2);

        list.add("two");

        Assert.assertEquals(list.size(), 3);
    }

    @Test
    public void addAllToNewTest(){

        CustomLinkedList listToCheck = new CustomLinkedList<>();

        CustomLinkedList listToAdd = new CustomLinkedList<>();

        listToAdd.add("one");

        listToAdd.add("two");

        listToCheck.addAll(listToAdd);

        Assert.assertEquals(listToCheck.size(), 2);
        Assert.assertEquals(listToCheck.get(0), "one");
        Assert.assertEquals(listToCheck.get(1), "two");

    }

    @Test
    public void addAllToPresentTest(){

        CustomLinkedList listToCheck = new CustomLinkedList();

        CustomLinkedList listToAdd = new CustomLinkedList<>();

        listToCheck.add("First element of custom list");

        listToAdd.add("one");

        listToAdd.add("two");

        listToCheck.addAll(listToAdd);

        Assert.assertEquals(listToCheck.size(), 3);
        Assert.assertEquals(listToCheck.get(0), "First element of custom list");
        Assert.assertEquals(listToCheck.get(1), "one");
        Assert.assertEquals(listToCheck.get(2), "two");
    }

    @Test
    public void removeTest(){

        CustomLinkedList list = new CustomLinkedList();

        list.add("one");

        list.add("two");

        list.remove("one");

        Assert.assertEquals(list.size(), 1);
        Assert.assertEquals(list.get(0), "two");
    }

    @Test
    public void isEmptyTest(){

        CustomLinkedList list = new CustomLinkedList();

        Assert.assertTrue(list.isEmpty());

        list.add("one");

        Assert.assertFalse(list.isEmpty());
    }

    @Test
    public void containsTest(){

        CustomLinkedList list = new CustomLinkedList();

        list.add("one");
        list.add("two");

        Assert.assertTrue(list.contains("one"));
        Assert.assertTrue(list.contains("two"));

        list.remove("one");
        Assert.assertFalse(list.contains("one"));
        Assert.assertTrue(list.contains("two"));
    }

    @Test
    public void getTest(){

        CustomLinkedList list = new CustomLinkedList();

        list.add("one");

        list.add("two");

        Assert.assertEquals(list.get(0), "one");
        Assert.assertEquals(list.get(1), "two");
    }
    @Test
    public void cumulativeTest(){

        CustomLinkedList list = new CustomLinkedList();

        list.add("one");

        list.add("two");

        Assert.assertEquals(list.size(), 2);
        Assert.assertEquals(list.get(0), "one");
        Assert.assertEquals(list.get(1), "two");

        list.remove("one");
        Assert.assertEquals(list.size(), 1);
        Assert.assertEquals(list.get(0), "two");

        list.add("three");
        Assert.assertEquals(list.size(), 2);
        Assert.assertEquals(list.get(0), "two");
        Assert.assertEquals(list.get(1), "three");

    }
}

