import CustomLinkedList.CustomLinkedList;
import org.testng.annotations.Test;

/**
 * Created by igorp on 6/07/17.
 */
public class PerformanceCustomLinkedListAddAllMethodTest extends BaseTest {
    double totalDefault;
    double totalCustom;
    double result;

    @Test
    public void test1_GetSpeedAddAllDefaultMethods (){

        long start = System.nanoTime();

        CustomLinkedList listToAdd = new CustomLinkedList<>();
        listToAdd.add("aggggggggggzahsxhsxdhj");
        listToAdd.add("aggggggggggzahsxhsxdhj");
        listToAdd.add("agggfewgedggggggggzahsxhsxdhj");
        listToAdd.add("agggggdsgbzasdggggggzahsxhsxdhj");
        listToAdd.add("agggggeswtgeswtggggggzahsxhsxdhj");
        listToAdd.add("aggggggg252525256gggzahsxhsxdhj");

        CustomLinkedList list = new CustomLinkedList<>();

        list.addAll(listToAdd);

        long finish = System.nanoTime();

        totalDefault = finish - start;

    }

    @Test
    public void test0_GetSpeedAddAllCustomMethods (){

        long start = System.nanoTime();

        CustomLinkedList listToAdd = new CustomLinkedList<>();
        listToAdd.add("aggggggggggzahsxhsxdhj");
        listToAdd.add("aggggggggggzahsxhsxdhj");
        listToAdd.add("agggfewgedggggggggzahsxhsxdhj");
        listToAdd.add("agggggdsgbzasdggggggzahsxhsxdhj");
        listToAdd.add("agggggeswtgeswtggggggzahsxhsxdhj");
        listToAdd.add("aggggggg252525256gggzahsxhsxdhj");

        CustomLinkedList list = new CustomLinkedList();

        list.addAll(listToAdd);

        long finish = System.nanoTime();

        totalCustom = finish - start;
    }

    @Test
    public void test2_SpeedResults (){

        System.out.println("Default linked list works " + totalDefault);
        System.out.println("Custom linked list works " + totalCustom);

        if(totalDefault<totalCustom){
            result = totalCustom / totalCustom;
            System.out.println("Default linked list is faster in " + result + " times");
        }
        else{
            result = totalCustom / totalDefault;
            System.out.println("Custom linked list is faster in " + result + " times, lol");
        }
    }
}

