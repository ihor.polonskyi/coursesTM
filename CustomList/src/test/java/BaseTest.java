import org.testng.ITestContext;
import org.testng.annotations.*;

/**
 * Created by igorp on 8/08/17.
 */
public class BaseTest {

    double totalDefault;
    double start;
    double finish;

    @BeforeSuite
    public void beforeSuite(final ITestContext testContext){

        System.out.println("\n Test suite started \n");
        start = System.currentTimeMillis();
    }

    @AfterSuite
    public void afterSuite(final ITestContext testContext){
        finish = System.currentTimeMillis();

        totalDefault = finish - start;

        System.out.println("Test suite finished, at " + testContext.getEndDate()
                + ", with time " + Math.round(totalDefault) + " milliseconds");
    }

    @BeforeTest
    public void beforeTest(final ITestContext testContext){
        System.out.println(testContext.getName() + " started" );
    }

    @AfterTest
    public void afterTest(final ITestContext testContext){
        System.out.println(testContext.getName() + " finished \n");
    }

    @AfterMethod
    public void afterMethod(final ITestContext testContext){
        System.out.println("Test from " + testContext.getName() + " finished");
    }
}
