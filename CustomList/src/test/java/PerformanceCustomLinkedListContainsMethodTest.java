import CustomLinkedList.CustomLinkedList;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by igorp on 6/07/17.
 */
public class PerformanceCustomLinkedListContainsMethodTest extends BaseTest {
    double totalDefault;
    double totalCustom;
    double result;

    @Test
    public void test1_GetSpeedAddAllDefaultMethods (){

        long start = System.nanoTime();

        List list = new LinkedList<>();
        list.add("aggggggggggzahsxhsxdhj");
        list.add("aggggggggggzahsxhsxdhj");
        list.add("agggfewgedggggggggzahsxhsxdhj");
        list.add("agggggdsgbzasdggggggzahsxhsxdhj");
        list.add("agggggeswtgeswtggggggzahsxhsxdhj");
        list.add("aggggggg252525256gggzahsxhsxdhj");

        Assert.assertTrue(list.contains("aggggggg252525256gggzahsxhsxdhj"));

        long finish = System.nanoTime();

        totalDefault = finish - start;

    }

    @Test
    public void test0_GetSpeedAddAllCustomMethods (){

        long start = System.nanoTime();

        CustomLinkedList list = new CustomLinkedList();
        list.add("aggggggggggzahsxhsxdhj");
        list.add("aggggggggggzahsxhsxdhj");
        list.add("agggfewgedggggggggzahsxhsxdhj");
        list.add("agggggdsgbzasdggggggzahsxhsxdhj");
        list.add("agggggeswtgeswtggggggzahsxhsxdhj");
        list.add("aggggggg252525256gggzahsxhsxdhj");

        Assert.assertTrue(list.contains("aggggggg252525256gggzahsxhsxdhj"));

        long finish = System.nanoTime();

        totalCustom = finish - start;
    }

    @Test
    public void test2_SpeedResults (){

        System.out.println("Default linked list works " + totalDefault);
        System.out.println("Custom linked list works " + totalCustom);

        if(totalDefault<totalCustom){
            result = totalCustom / totalCustom;
            System.out.println("Default linked list is faster in " + result + " times");
        }
        else{
            result = totalCustom / totalDefault;
            System.out.println("Custom linked list is faster in " + result + " times, lol");
        }
    }


}

