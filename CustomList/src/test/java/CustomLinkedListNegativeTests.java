import CustomLinkedList.CustomLinkedList;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by igorp on 6/07/17.
 */
public class CustomLinkedListNegativeTests extends BaseTest {


    @Test(expectedExceptions = IllegalStateException.class,
            expectedExceptionsMessageRegExp = "Collection is empty")
    public void addAllToNewTest(){

        CustomLinkedList listToCheck = new CustomLinkedList();
        CustomLinkedList listToAdd = new CustomLinkedList<>();

        listToCheck.addAll(listToAdd);
    }


    @Test(expectedExceptions = NullPointerException.class,
    expectedExceptionsMessageRegExp = "Element to delete not found")
    public void removeTest(){

        CustomLinkedList list = new CustomLinkedList();

        list.add("one");

        list.add("two");

        list.remove("three");
    }

    @Test(expectedExceptions = IllegalStateException.class,
            expectedExceptionsMessageRegExp = "Cannot find element in empty list.")
    public void containsTest(){

        CustomLinkedList list = new CustomLinkedList();

        Assert.assertTrue(list.contains("one"));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void getTest(){

        CustomLinkedList list = new CustomLinkedList();

        list.add("one");

        list.add("two");

        list.get(3).toString();
    }
}

