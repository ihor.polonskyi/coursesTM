package CustomLinkedList;

import java.util.*;


public class CustomLinkedList <T> implements Iterable<T> {

    private Node first;
    private Node last;
    private int size;

    public CustomLinkedList() {
        first = null;
        last = null;
        size = 0; //size of list
    }

    public void add(T item) {
        if (item == null)
            throw new IllegalStateException("Argument to add is null");

        if (!isEmpty()) {
            Node prev = last;
            last = new Node(item);
            prev.next = last;
        } else {
            last = new Node(item);
            first = last;
        }
        size++;
    }

    public void addAll(CustomLinkedList<T> c) {
        if(c.isEmpty())
            throw new IllegalStateException("Collection is empty");

        c.forEach(element ->{

            if (!isEmpty()) {
                Node prev = last;
                last = new Node(element);
                prev.next = last;
            } else {
                last = new Node(element);
                first = last;
            }
            size++;
        });
    }

    public boolean remove(Object item) {
        if (isEmpty())
            throw new IllegalStateException("Cannot remove() from an empty list.");

        boolean result = false;
        Node prev = first;
        Node curr = first;
        try{
            while (curr.next != null || curr == last) {
                if (curr.data.equals(item)) {
                    if (size == 1) {
                        first = null;
                        last = null;
                    }
                    //for first element
                    else if (curr.equals(first)) {
                        first = first.next;
                    }
                    //for last element
                    else if (curr.equals(last)) {
                        last = prev;
                        last.next = null;
                    }
                    //remove usual element
                    else {
                        prev.next = curr.next;
                    }
                    size--;
                    result = true;
                    break;
                }
                prev = curr;
                curr = prev.next;
            }
        }
        catch (NullPointerException e){
            throw new NullPointerException("Element to delete not found");
        }

        return result;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean contains(T item) {
        if (isEmpty())
            throw new IllegalStateException("Cannot find element in empty list.");

        Node curr = first;
        int count = 0;

        while (curr != null)
        {
            if (curr.data.equals(item))
                return true;
            count++;
            curr = curr.next;
        }
        return false;
        }


    public T get(int index){
        if (isEmpty())
            throw new IllegalStateException("Cannot get element form empty list");

        Node curr = first;
        int count = 0;

        while (curr != null)
            {
                if (count == index)
                    return curr.data;
                count++;
                curr = curr.next;
            }

        return null;
    }

        private class Node {
            private T data;
            private Node next;

            public Node(T data) {
                this.data = data;
            }
        }

        public Iterator<T> iterator () {
            return new LinkedListIterator();
        }

        private class LinkedListIterator implements Iterator<T> {
            private Node current = first;

            public T next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                T item = current.data;
                current = current.next;
                return item;
            }

            public boolean hasNext() {
                return current != null;
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        }
}


