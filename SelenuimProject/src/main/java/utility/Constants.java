package utility;

/**
 * Created by igorp on 11/05/17.
 */
public class Constants {

    public static final String URL = "https://www.templatemonster.com";
    public static final String CLEAN_CART_URL = URL + "/cart.php?rid=clearcart";
    public static final String AUTORIZATION_URL = "https://account.templatemonster.com/auth/#/";
    public static final String ON_CART_OFFER_NAME = "Fast";
    public static final String ON_TEMPLATE_OFFER_NAME = "Installation";

    public static final int PAGE_TIMEOUT = 60;
    public static final int ELEMENT_TIMEOUT = 20;
}
