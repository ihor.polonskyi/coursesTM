package utility.services;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import static utility.Log.info;
import static utility.services.ReportService.assertTrue;

public class CookiesService {

    private static WebDriver driver;
    public CookiesService(WebDriver driver){
        this.driver = driver;
    }

    public static String getCookieValue(String cookieName){
        if (driver.manage().getCookieNamed(cookieName)!=null){
            String value = driver.manage().getCookieNamed(cookieName).getValue();
            info("Cookie: \"" + cookieName + "\" has value - \"" + value + "\".");
            return value;
        }
        else {
            assertTrue(false, "Cookie: \"" + cookieName + "\" was not found after timeout.");
            return null;
        }
    }

    public static void addCookie(String name, String value){
        Cookie.Builder builder = new Cookie.Builder(name, value);
        Cookie cookie = builder.build();
        driver.manage().addCookie(cookie);
        info("Add cookie "+name+" with value "+value);
    }
}