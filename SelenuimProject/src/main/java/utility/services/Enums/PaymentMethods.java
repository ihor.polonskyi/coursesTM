package utility.services.Enums;

/**
 * Created by igorp on 4/09/17.
 */
public enum PaymentMethods {
    PayPal,
    Stripe,
    Skrill,
    PayProGlobal
}
