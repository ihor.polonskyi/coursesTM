package utility.services.Enums;

/**
 * Created by igorp on 5/09/17.
 */
public enum OfferTypes {
    OnCart,
    OnTemplate
}
