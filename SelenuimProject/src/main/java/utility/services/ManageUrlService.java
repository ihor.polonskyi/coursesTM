package utility.services;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import utility.Constants;
import utility.Log;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static utility.Log.info;
import static utility.services.CookiesService.addCookie;
import static utility.services.ReportService.assertTrue;
import static utility.services.WaiterService.waitCssLoad;
import static utility.services.WebElementService.moveToCoordinate;

public class ManageUrlService {

    private static WebDriver driver;
    public ManageUrlService(WebDriver driver){
        this.driver = driver;
    }

    public static void getURL(String url) {
        driver.getCurrentUrl();
        info("Navigate to \""+url+"\".");
        try {
            driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
            driver.get(url);

            info("Navigate to \""+url+"\" finished.");
            moveToCoordinate(0,0);
        }
        catch (TimeoutException e){
            stopLoad();
        }
    }

    public  static void refreshPage(){
        try {
            driver.navigate().refresh();
            info("Page was refreshed.");
        }
        catch (WebDriverException e){
            stopLoad();
        }
    }

    public  static String getCurrentURL(){
        info("Current URL:"+driver.getCurrentUrl());
        return driver.getCurrentUrl();
    }


    public static void switchToLastWindow(){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    return (driver.getWindowHandles().size() > 1) ;
                }
            });
            for (String win:driver.getWindowHandles()){
                driver.switchTo().window(win);
            }
            info("Switch to another window.");
        }
        catch (TimeoutException e){
            assertTrue(false, "You have only one window.");
        }
        driver.manage().window().maximize();
        info("Maximize window.");
    }

    public static void switchToLastWindowClose(){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    return (driver.getWindowHandles().size() > 1) ;
                }
            });
            String last = "";
            Iterator<String> iterator = driver.getWindowHandles().iterator();
            while (iterator.hasNext()){
               last=iterator.next();
            }
            for (String win:driver.getWindowHandles()){
                if (win.equals(last)) {
                    driver.switchTo().window(win);
                }
                else driver.close();
            }
            info("Switch to another window.");
        }
        catch (TimeoutException e){
            assertTrue(false, "You have only one window.");
        }
        driver.manage().window().maximize();
        info("Maximize window.");
    }


    public static void scrollDown(){
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("window.scrollBy(0,document.body.scrollHeight)", "");
    }

    public static void stopLoad(){
        driver.findElement(By.tagName("body")).sendKeys(Keys.ESCAPE);
        info("Timeout on loading page \""+driver.getCurrentUrl()+"\".");
    }


    public static void scrollDown(int px){
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("window.scrollBy(0,"+px+")", "");
    }

    public static void switchToWindow(){
        for (String win:driver.getWindowHandles()){
            driver.switchTo().window(win);
        }
        info("Switch to another window.");
        driver.manage().window().maximize();
        info("Maximize window.");
    }

    public static void navigateBack (){
        try {
            driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
            driver.navigate().back();
            info("Returned to the previous page.");
        }
        catch (TimeoutException e){
            stopLoad();
        }
    }

    public static void clearCart(){
        getURL(Constants.CLEAN_CART_URL);
        waitCssLoad();
        Log.info("Card was cleared");
        navigateBack();
    }

    public static void getUrlWithSettingCookies(String url, List<String> cookies){
        driver.getCurrentUrl();
        info("Navigate to \""+url+"\".");
        try {
            driver.get(url);

            info("Navigate to \""+url+"\" finished.");
            moveToCoordinate(0,0);

            cookies.forEach(cookie ->{
                addCookie(cookie, "1");
            });

            refreshPage();
        }
        catch (TimeoutException e){
            stopLoad();
        }
    }
}


