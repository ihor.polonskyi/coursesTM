package utility.services;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import utility.Log;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 19.05.17.
 */
public class WebDriverFactory {

    public static WebDriver getDriverInstance(String browserName, String nodeName) throws MalformedURLException{
        WebDriver driver;
        switch (browserName){
            case BrowserType.CHROME:
                driver = new RemoteWebDriver(new URL(nodeName), getChromeDesiredCapabilities());
                break;
            case BrowserType.FIREFOX:
                driver = new RemoteWebDriver(new URL(nodeName),  getFirefoxDesiredCapabilities());
                break;
            default:
                driver = new RemoteWebDriver(new URL(nodeName), getChromeDesiredCapabilities());
                break;
        }
        return driver;
    }

    private static DesiredCapabilities getChromeDesiredCapabilities() {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();

        desiredCapabilities.setJavascriptEnabled(true);
        desiredCapabilities.setCapability(CapabilityType.SUPPORTS_APPLICATION_CACHE, true);
        desiredCapabilities.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
        desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, addChromeOptions());

        return desiredCapabilities;
    }

    private static ChromeOptions addChromeOptions() {

        Map<String, Object> prefs = new HashMap<>();
        prefs.put("credentials_enable_service", false);
        prefs.put("profile.password_manager_enabled", false);
        prefs.put("profile.default_content_setting_values.notifications", 2);

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("prefs", prefs);
        chromeOptions.addArguments("start-maximized");
        Log.info("Maximize window.");

        return chromeOptions;
    }

    private static DesiredCapabilities getFirefoxDesiredCapabilities() {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.firefox();
        desiredCapabilities.setJavascriptEnabled(true);
        desiredCapabilities.setCapability(CapabilityType.SUPPORTS_APPLICATION_CACHE, true);
        desiredCapabilities.setCapability(CapabilityType.TAKES_SCREENSHOT, true);

        return desiredCapabilities;
    }

}
