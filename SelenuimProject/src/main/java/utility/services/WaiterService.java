package utility.services;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.SitePages.IndexPage;
import utility.Constants;

import static org.openqa.selenium.support.PageFactory.initElements;
import static utility.Constants.ELEMENT_TIMEOUT;
import static utility.Log.info;
import static utility.Log.warn;
import static utility.services.ReportService.assertTrue;
import static utility.services.WebElementService.elementIsDisplayed;

@Log4j
public class WaiterService {

    private static WebDriver driver;
    public WaiterService(WebDriver driver){
        this.driver = driver;
    }

    public static void waitForCookie(String cookieName){

        int attempt_counter = 0;
        while (driver.manage().getCookieNamed(cookieName)==null ||
                (driver.manage().getCookieNamed(cookieName) != null && driver.manage().getCookieNamed(cookieName).getValue() == null))
        {
            WaiterService.sleep(1);
            attempt_counter++;
            if (attempt_counter == ELEMENT_TIMEOUT){
                warn("Cookie: \"" + cookieName + "\" was NOT found, break by attempt counter.");
                break;}
        }
    }

    @Deprecated
    public static void waitForElementDisappear(WebElement element, String elementName){

        info("Wait for element: \"" + elementName + "\" disappear.");
        int attempt_counter = 0;
        while (elementIsDisplayed(element,elementName)) {
            sleep(1);
            attempt_counter++;
            if (attempt_counter == 5){
                assertTrue(false, "\"" + elementName + "\" not disappeared after timeout.");
                break;
            }

        }
    }

    public static void waitForElementVisible(WebElement element) {

        try {
            WebDriverWait wait = new WebDriverWait(driver,20);
            wait.until(ExpectedConditions.visibilityOf(element));
        }
        catch (TimeoutException e){
            assertTrue(false, "ELEMENT: \"" + element + "\" is not presents.");
        }
        catch (StaleElementReferenceException e){
            warn("ELEMENT: \"" + element + "\" is not found in the cache.");
        }

    }

    public static void waitForElementVisible(WebElement element, int delay) {

        try {
            WebDriverWait wait = new WebDriverWait(driver,delay);
            wait.until(ExpectedConditions.visibilityOf(element));
        }
        catch (TimeoutException e){
            warn("ELEMENT: \"" + element + "\" is not presents.");
        }

    }

    public static void sleep(int seconds){

        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitPageLoader(String url) {
        waitPageLoader(url, Constants.PAGE_TIMEOUT);
    }

    public static void waitPageLoader(String url, int seconds) {

        try {
            info("Waiting for \"" + url + "\" page.");
            int attempt = 0;
            while (!driver.getCurrentUrl().contains(url) && attempt < seconds) {
                attempt++;
                sleep(1);
            }
            info("Waiting for \"" + url + "\" page during " + attempt + " seconds.");
            if (!driver.getCurrentUrl().contains(url)) {
                assertTrue(false, "Expected page hasn't loaded  by timeout.\n" +
                        "                                   Current url:" + driver.getCurrentUrl());
            }
        } catch (TimeoutException e) {
            ManageUrlService.stopLoad();
        }
    }

    public static void waitForElementDisappear(WebElement element, int seconds, String elementName){

        info("Wait for element: \"" + elementName + "\" disappear.");
        int attempt_counter = 0;
        while (elementIsDisplayed(element,elementName)) {
            sleep(1);
            attempt_counter++;
            if (attempt_counter == seconds){
                assertTrue(false, "\"" + elementName + "\" not disappeared after timeout.");
                break;
            }

        }
    }

    public static void waitPageLoader(){

        try {
            info("Waiting for change url.");
            final String previousUrl = driver.getCurrentUrl();
            WebDriverWait wait = new WebDriverWait(driver, Constants.PAGE_TIMEOUT);
            wait.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver driver) {
                    return (!driver.getCurrentUrl().equals(previousUrl));
                }
            });
        }
        catch (TimeoutException e){
            assertTrue(false, "Expected page hasn't changed  by timeout.\n" +
                    "                                   Current url:"+driver.getCurrentUrl());
        }
    }

    public static void waitForElementClickable(WebElement element) {

        try {
            WebDriverWait wait = new WebDriverWait(driver,20);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }
        catch (TimeoutException e){
            assertTrue(false, "ELEMENT: \"" + element + "\" is not clickable.");
        }
        catch (StaleElementReferenceException e){
            warn("ELEMENT: \"" + element + "\" is not found in the cache.");
        }
    }

    public static void waitCssLoad(){
        IndexPage indexPage = initElements(driver, IndexPage.class);
        waitForElementClickable(indexPage.favorites);
    }

    public static void waitForTextIsPresent(WebElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, Constants.ELEMENT_TIMEOUT);
            wait.until((WebDriver webDriver) -> !element.getText().isEmpty());
            info("Text is present in element");
        }
        catch (TimeoutException e){
            assertTrue(false, "Text is not present in element");
        }
        catch (NoSuchElementException e){
            assertTrue(false, "This element not found.");
        }

    }

    public static void waitPageLoaderJs() {
        try {
            new WebDriverWait(driver, Constants.PAGE_TIMEOUT).until((WebDriver webDriver) ->
                    ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
        } catch (WebDriverException e) {
            warn(e);
        }
    }

    public static void waitJqueryComplete(){
        WebDriverWait wait = new WebDriverWait(driver, Constants.ELEMENT_TIMEOUT);
        try {
            wait.until((WebDriver webDriver) -> ((JavascriptExecutor)driver).executeScript("return jQuery.active").toString().equals("0"));
        } catch (TimeoutException e) {
            info("No one jQuery activity or activity continues");
        }
    }
}
