package pageobjects.PaymentsPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static utility.services.WaiterService.waitForTextIsPresent;
import static utility.services.WebElementService.getElementText;

/**
 * Created by igorp on 4/09/17.
 */
public class PayPalPage {

    final WebDriver driver;

    public PayPalPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id='orderSummaryTable']//td/strong")
    public WebElement orderSummary;

    public String getOrderAmount(){
        waitForTextIsPresent(orderSummary);
        return getElementText(orderSummary, "Order summary").replace(".00","").replaceAll("\\D","");
    }
}
