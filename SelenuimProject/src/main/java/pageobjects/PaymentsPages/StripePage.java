package pageobjects.PaymentsPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static utility.services.WaiterService.waitForTextIsPresent;
import static utility.services.WebElementService.getElementText;

/**
 * Created by igorp on 5/09/17.
 */
public class StripePage {

    final WebDriver driver;

    public StripePage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "s-amount")
    public WebElement orderSummary;

    public String getOrderAmount(){
        waitForTextIsPresent(orderSummary);
        return getElementText(orderSummary, "Order summary").replaceAll("\\D","");
    }
}