package pageobjects.SitePages;

import businessobjects.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.ForeignPages.FacebookLoginPopUpPage;
import utility.Constants;

import static org.openqa.selenium.support.PageFactory.initElements;
import static utility.services.ManageUrlService.switchToLastWindow;
import static utility.services.ManageUrlService.switchToWindow;
import static utility.services.WaiterService.waitForElementClickable;
import static utility.services.WaiterService.waitPageLoader;
import static utility.services.WebElementService.clickOnElement;
import static utility.services.WebElementService.sendKeys;

/**
 * Created by igorp on 22/08/17.
 */
public class AuthPage {

    final WebDriver driver;

    public AuthPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//input[@type='email']")
    public WebElement emailField;

    @FindBy(xpath = "//input[@type='password']")
    public WebElement passwordField;

    @FindBy(xpath = "//*[contains(@id,'continue')]//button")
    public WebElement continueButton;

    @FindBy(xpath = "//*[contains(@id,'login')]//button")
    public WebElement logInButton;

    @FindBy(className = "app-logo")
    public WebElement tmLogo;

    @FindBy(xpath = "//*[contains(@id,'facebook-button')]")
    public WebElement facebookLoginButton;

    public void loginWithRedirectToIndexPage(User user){

        waitForElementClickable(emailField);
        sendKeys(emailField, "Email field", user.getEmail());
        clickOnElement(continueButton, "Continue button");

        waitForElementClickable(passwordField);
        sendKeys(passwordField, "Password field", user.getPassword());
        clickOnElement(logInButton, "Continue button");

        //huck to get index page
        waitPageLoader("/downloads");
        waitForElementClickable(tmLogo);
        clickOnElement(tmLogo, "TM logo");
        waitPageLoader(Constants.URL);
    }

    public void loginWithRedirectToBillingPage(User user){

        waitForElementClickable(emailField);
        sendKeys(emailField, "Email field", user.getEmail());
        clickOnElement(continueButton, "Continue button");

        waitForElementClickable(passwordField);
        sendKeys(passwordField, "Password field", user.getPassword());
        clickOnElement(logInButton, "Continue button");
    }

    public void loginWithRedirectToIndexPageByFacebook(User facebookUser){

        waitForElementClickable(facebookLoginButton);
        clickOnElement(facebookLoginButton, "Facebook login button");
        switchToLastWindow();

        FacebookLoginPopUpPage facebookLoginPopUpPage = initElements(driver, FacebookLoginPopUpPage.class);
        facebookLoginPopUpPage.login(facebookUser);

        switchToWindow();

        //huck to get index page
        waitPageLoader("/downloads");
        waitForElementClickable(tmLogo);
        clickOnElement(tmLogo, "TM logo");
        waitPageLoader(Constants.URL);
    }

    public void loginWithRedirectToBillingPageByFacebook(User facebookUser){

        waitForElementClickable(facebookLoginButton);
        clickOnElement(facebookLoginButton, "Facebook login button");
        switchToLastWindow();

        FacebookLoginPopUpPage facebookLoginPopUpPage = initElements(driver, FacebookLoginPopUpPage.class);
        facebookLoginPopUpPage.login(facebookUser);
        switchToLastWindow();
    }



}
