package pageobjects.SitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.Log;
import utility.services.Enums.OfferTypes;

import java.util.List;
import java.util.Random;

import static org.openqa.selenium.support.PageFactory.initElements;
import static utility.services.WaiterService.waitForElementClickable;
import static utility.services.WaiterService.waitJqueryComplete;
import static utility.services.WebElementService.*;

/**
 * Created by igorp on 1/09/17.
 */
public class CartPopUpAndItemInfoPage {

    final WebDriver driver;
    Random random = new Random();

    public CartPopUpAndItemInfoPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//*[contains(@class,'js-total-price')]")
    public WebElement orderTotalPrice;

    @FindBy(xpath = "//*[contains(@class,'recommended')]//strong[contains(@class,'service-name')]")
    public List<WebElement> onTemplateOffersNamesList;

    @FindBy(xpath = "//*[contains(@class,'recommended')]//*[contains(@id,'add-offer')]")
    public List<WebElement> onTemplateOffersButtonsList;

    @FindBy(xpath = "//*[contains(@class,'upsells')]//strong[contains(@class,'service-name')]")
    public List<WebElement> onCartOffersNamesList;

    @FindBy(xpath = "//*[contains(@class,'upsells')]//*[contains(@id,'add-offer')]")
    public List<WebElement> onCartOffersButtonsList;

    @FindBy(id = "cart-summary-checkout")
    public WebElement checkoutNowButton;

    public String getOrderTotalPrice(){
        return getElementAttribute(orderTotalPrice, "Order total price", "data-price");
    }

    public void selectRandomOffer(OfferTypes offerType){
        CartPopUpAndItemInfoPage cartPopUpAndItemInfoPage = initElements(driver, CartPopUpAndItemInfoPage.class);
        waitJqueryComplete();

        switch (offerType){
            case OnCart:
                if(onCartOffersButtonsList.isEmpty()){
                    Log.warn("Template has not On Cart offers");
                    break;
                }
                clickOnRandomTemplate(onCartOffersButtonsList);
                waitForElementClickable(cartPopUpAndItemInfoPage.checkoutNowButton);
                break;

            case OnTemplate:
                if(onTemplateOffersButtonsList.isEmpty()){
                    Log.warn("Template has not On Template offers");
                    break;
                }
                clickOnRandomTemplate(onTemplateOffersButtonsList);
                waitForElementClickable(cartPopUpAndItemInfoPage.checkoutNowButton);
                break;

            default:
                throw new IllegalArgumentException("Incorrect offer name, choose OnCart or OnTemplate offer name");
        }
    }

    private void clickOnRandomTemplate(List<WebElement> buttons){
        int numberOnCard = random.nextInt(buttons.size());
        waitForElementClickable(buttons.get(numberOnCard));
        clickOnElement(buttons.get(numberOnCard), "Random on card offer");

        Log.info("Select random cart offer");
        waitJqueryComplete();
    }

    boolean isSelected;
    public void selectOffer(String offerType, String offerName) {
        isSelected = false;
        waitJqueryComplete();
        switch (offerType) {
            case "OnCart":
                for(int i = 0; i < onCartOffersNamesList.size(); i++){
                    if(getElementText(onCartOffersNamesList.get(i), "On cart offer").contains(offerName)){
                        clickOnTemplate(i, offerName, onCartOffersButtonsList);
                        break;
                    }
                }
                if(!isSelected)
                    Log.warn("On cart offer with name: " + offerName + " not found");
                break;

            case "OnTemplate":
                for(int i = 0; i < onTemplateOffersNamesList.size(); i++){
                    if(getElementText(onTemplateOffersNamesList.get(i), "On template offer").contains(offerName)){
                        clickOnTemplate(i, offerName, onTemplateOffersButtonsList);
                        break;
                    }
                }
                if(!isSelected)
                    Log.warn("On template offer with name: " + offerName + " not found");
                break;
        }
    }

    private void clickOnTemplate(int i, String offername, List<WebElement> buttons){
        waitForElementClickable(buttons.get(i));
        clickOnElement(buttons.get(i), "On cart Offer Button");
        Log.info("Click on cart offer with name: " + offername);
        waitJqueryComplete();
        isSelected = true;
    }

    public void clickCheckoutNowButton() {
        clickOnElement(checkoutNowButton, "Checkout now button");
    }

}
