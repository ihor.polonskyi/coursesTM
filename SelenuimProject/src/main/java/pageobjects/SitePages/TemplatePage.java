package pageobjects.SitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.Log;

import static org.openqa.selenium.support.PageFactory.initElements;
import static utility.services.WaiterService.waitForElementClickable;
import static utility.services.WebElementService.*;

/**
 * Created by igorp on 1/09/17.
 */
public class TemplatePage {

    final WebDriver driver;

    public TemplatePage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//*[contains(@id,'preview-add-to-cart')]")
    public WebElement getItNowButton;

    @FindBy(xpath = "//*[contains(@class,'info__name')]")
    public WebElement templateName;

    @FindBy(xpath = "//*[contains(@id,'preview-add-to-cart-regular')]")
    public WebElement addToCartButton;

    public void clickOnGetItNowButton(){
        CartPopUpAndItemInfoPage cartPopUpAndItemInfoPage = initElements(driver,CartPopUpAndItemInfoPage.class);

        if(elementIsDisplayed(getItNowButton, "New template page add to cart button")){
            waitForElementClickable(getItNowButton);
            clickOnElement(getItNowButton, "Get It Now Button");
            waitForElementClickable(cartPopUpAndItemInfoPage.checkoutNowButton);
        }
        else{
            waitForElementClickable(addToCartButton);
            Log.info("Old template page");
            clickOnElement(addToCartButton, "Old template page add to cart button");
            waitForElementClickable(cartPopUpAndItemInfoPage.checkoutNowButton);
        }
    }

    @FindBy(xpath = "//*[contains(@class,'checkout-total')]//*[contains(@class,'js-template-price')]")
    public WebElement templatePrice;

    public String getTemplatePrice(){
        return getElementText(templatePrice, "Template price on template page").substring(1);
    }

    public String getTemplateName() {
        return getElementText(templateName, "Template name");
    }
}
