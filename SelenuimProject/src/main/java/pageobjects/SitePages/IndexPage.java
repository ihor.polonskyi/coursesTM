package pageobjects.SitePages;

import businessobjects.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.openqa.selenium.support.PageFactory.initElements;
import static utility.services.ManageUrlService.switchToLastWindowClose;
import static utility.services.WaiterService.waitCssLoad;
import static utility.services.WebElementService.*;

/**
 * Created by igorp on 19/08/17.
 */
public class IndexPage {

    final WebDriver driver;

    public IndexPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "header-signin-link")
    public WebElement accountLink;

    @FindBy(id = "user-avatar")
    public WebElement userAvatar;

    @FindBy(id = "header-signOut-link")
    public WebElement signOutLink;

    @FindBy(id = "menu-favorites")
    public WebElement favorites;

    @FindBy(xpath = "//*[contains(@id,'thmb-img')]")
    public List<WebElement> templatePreviewsList;

    @FindBy(name = "keywords")
    public WebElement searchField;

    public WebElement searchFormSubmit;

    public void clickAccountLink(){
        clickOnElement(accountLink, "Account link");
        switchToLastWindowClose();
    }

    public void logout(){
        clickOnElement(userAvatar, "Account menu");
        clickOnElement(signOutLink, "Sign out link");
    }

    public void logInWithRedirectToBillingPage(User user){
        clickAccountLink();

        AuthPage authPage = initElements(driver, AuthPage.class);
        authPage.loginWithRedirectToBillingPage(user);
    }

    public void logInWithRedirectToIndexPage(User user){
        clickAccountLink();

        AuthPage authPage = initElements(driver, AuthPage.class);
        authPage.loginWithRedirectToIndexPage(user);
    }

    public void logInWithRedirectToBillingPageByFacebook(User facebookUser){
        clickAccountLink();

        AuthPage authPage = initElements(driver, AuthPage.class);
        authPage.loginWithRedirectToBillingPageByFacebook(facebookUser);
    }

    public void logInWithRedirectToIndexPageByFacebook(User facebookUser){
        clickAccountLink();

        AuthPage authPage = initElements(driver, AuthPage.class);
        authPage.loginWithRedirectToIndexPageByFacebook(facebookUser);
    }


    public void goToRandomTemplatePage(){
        Random random = new Random();

        List<String> templatesIdList = templatePreviewsList.stream()
                .map(element -> getElementAttribute(element, "Template Preview", "data-tid"))
                .collect(Collectors.toList());

        search(templatesIdList.get(random.nextInt(templatesIdList.size())));
    }

    public void search(String text){
        sendKeysClear(searchField, "Search field", text);
        clickOnElement(searchFormSubmit, "Search form button");
        waitCssLoad();
    }



}
