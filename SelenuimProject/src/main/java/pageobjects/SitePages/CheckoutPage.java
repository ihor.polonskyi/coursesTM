package pageobjects.SitePages;

import businessobjects.User;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.Log;
import utility.services.Enums.PaymentMethods;

import java.util.List;

import static utility.services.WaiterService.waitForElementClickable;
import static utility.services.WebElementService.*;

/**
 * Created by igorp on 4/09/17.
 */
public class CheckoutPage {

    final WebDriver driver;

    public CheckoutPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//*[contains(@id,'checkout-payment-buy')]")
    public List<WebElement> paymentTypesList;

    @FindBy(name = "email")
    public WebElement emailField;

    @FindBy(name = "fullName")
    public WebElement fullNameField;

    @FindBy(xpath = "//*[contains(@id,'form_country')]")
    public WebElement countryDropdown;

    @FindBy(xpath = "//*[contains(@id,'country')]//*[@class='chosen-search']//input")
    public WebElement countrySubmitField;

    @FindBy(xpath = "//div[contains(@class,'chosen-phone')]//a")
    public WebElement phoneCodeDropdown;

    @FindBy(xpath = "//*[contains(@id,'phone')]//*[@class='chosen-search']//input")
    public WebElement phoneCodeSubmitCountryField;

    @FindBy(name = "phone")
    public WebElement phoneField;

    @FindBy(name = "postalCode")
    public WebElement postalCode;

    @FindBy(xpath = "//*[contains(@id,'form_stateiso2_chosen')]")
    public WebElement stateDropdown;

    @FindBy(xpath = "//*[contains(@id,'state')]//*[@class='chosen-search']//input")
    public WebElement stateSubmitField;

    @FindBy(name = "cityName")
    public WebElement city;

    @FindBy(name = "submitButton")
    public WebElement submitButton;

    private void insertEmail(User user){
        waitForElementClickable(emailField);
        sendKeysClear(emailField, "Email field", user.getEmail());
        emailField.sendKeys(Keys.ENTER);
        Log.info("Inserted email and press Enter");
    }

    private void insertFullName(User user){
        waitForElementClickable(fullNameField);
        sendKeys(fullNameField, "Full name field", user.getUserName());
    }

    private void insertCity(User user){
        waitForElementClickable(city);
        moveToElement(city, "City field");
        sendKeys(city, "City name field", user.getCity());
    }

    private void insertPostalCode(User user){
        waitForElementClickable(postalCode);
        sendKeys(postalCode, "Postal code field", user.getPostalCode());
    }

    private void insertCountry(User user){
        clickOnElement(countryDropdown, "Country dropdown");

        waitForElementClickable(countrySubmitField);
        sendKeys(countrySubmitField, "Country Submit Field", user.getCountry());
        countrySubmitField.sendKeys(Keys.ENTER);
        Log.info("Inserted country and press Enter");
    }

    private void insertPhone(User user){
        clickOnElement(phoneCodeDropdown, "Phone code dropdown");

        waitForElementClickable(phoneCodeSubmitCountryField);
        sendKeys(phoneCodeSubmitCountryField, "Phone code dropdown country field", user.getCountry());
        phoneCodeSubmitCountryField.sendKeys(Keys.ENTER);
        Log.info("Inserted country and press Enter");

        waitForElementClickable(phoneField);
        sendKeys(phoneField, "Insert phone field", user.getPhone());
    }

    private void insertState(User user){
        clickOnElement(stateDropdown, "State dropdown");
        waitForElementClickable(stateSubmitField);
        sendKeys(stateSubmitField, "Country Submit Field", user.getState());
        stateSubmitField.sendKeys(Keys.ENTER);
        Log.info("Inserted state and press Enter");
    }

    private void clickOnSubmitButton(){
        waitForElementClickable(submitButton);
        clickOnElement(submitButton, "Submit button");
    }

    public void insertNewUserData(User user){
        insertEmail(user);
        insertFullName(user);
        insertCountry(user);
        insertPhone(user);
        insertCity(user);
        insertPostalCode(user);
        if(user.getCountry().equals("United States"))
            insertState(user);
        clickEnterOnField(postalCode);
    }

    public void payBy(PaymentMethods paymentMethod){
       waitForElementClickable(paymentTypesList.get(0));
       WebElement payment = driver.findElement(By.id("checkout-payment-buy-"+paymentMethod));

       clickOnElement(payment, paymentMethod + " button");
       Log.info("Pay by "+paymentMethod);
    }

    public void clickEnterOnField(WebElement element){
        element.sendKeys(Keys.ENTER);
        Log.info("Click enter to last field");
    }

}
