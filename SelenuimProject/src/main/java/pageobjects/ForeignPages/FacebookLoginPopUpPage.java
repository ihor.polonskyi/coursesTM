package pageobjects.ForeignPages;

import businessobjects.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static utility.services.WaiterService.waitForElementClickable;
import static utility.services.WebElementService.clickOnElement;
import static utility.services.WebElementService.sendKeysClear;

/**
 * Created by igorp on 30/08/17.
 */
public class FacebookLoginPopUpPage {

    final WebDriver driver;

    public FacebookLoginPopUpPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(name = "email")
    public WebElement emailField;

    @FindBy(name = "pass")
    public WebElement passwordField;

    @FindBy(name = "login")
    public WebElement loginButton;

    @FindBy(id = "forgot-password-link")
    public WebElement forgotPasswordLink;

    public void login(User facebookUser){
        waitForElementClickable(emailField);
        sendKeysClear(emailField, "Email field", facebookUser.getEmail());
        sendKeysClear(passwordField, "Password field", facebookUser.getPassword());

        clickOnElement(loginButton, "Login Button");
    }
}
