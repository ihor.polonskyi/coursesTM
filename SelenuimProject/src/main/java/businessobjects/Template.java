package businessobjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import utility.services.PropertyReader;

/**
 * Created by igorp on 1/09/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Template{

    private String templateName;
    private String templateId;
    private String templatePrice;

    public Template(String fileLocation) {
        PropertyReader propertyReader = new PropertyReader(fileLocation);
        this.templateName = propertyReader.getValue("templateName");
        this.templateId = propertyReader.getValue("templateId");
        this.templatePrice = propertyReader.getValue("templatePrice");
    }

}