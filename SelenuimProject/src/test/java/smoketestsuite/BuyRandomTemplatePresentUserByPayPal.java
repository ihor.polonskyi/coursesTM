package smoketestsuite;

import businessobjects.Order;
import org.testng.annotations.Test;
import pageobjects.PaymentsPages.PayPalPage;
import pageobjects.SitePages.CartPopUpAndItemInfoPage;
import pageobjects.SitePages.CheckoutPage;
import pageobjects.SitePages.IndexPage;
import pageobjects.SitePages.TemplatePage;
import utility.Constants;
import utility.services.Enums.OfferTypes;
import utility.services.Enums.PaymentMethods;

import static org.openqa.selenium.support.PageFactory.initElements;
import static utility.services.ManageUrlService.*;
import static utility.services.ReportService.assertEquals;
import static utility.services.ReportService.assertTrue;
import static utility.services.WaiterService.*;

/**
 * Created by igorp on 1/09/17.
 */
public class BuyRandomTemplatePresentUserByPayPal extends BaseTest{
    Order order = new Order();

    @Test
    public void buyRandomTemplatePresentUserByPayPal() {

        //go to tm.com
        getUrlWithSettingCookies(Constants.URL, cookies);
        waitCssLoad();

        //login by present user
        IndexPage indexPage = initElements(driver, IndexPage.class);
        indexPage.logInWithRedirectToIndexPage(userEmail);
        waitCssLoad();
        waitForElementVisible(indexPage.userAvatar);

        //clear cart
        clearCart();
        waitCssLoad();

        //get random template page
        indexPage.goToRandomTemplatePage();

        //click on get it now button
        TemplatePage templatePage = initElements(driver, TemplatePage.class);
        templatePage.clickOnGetItNowButton();
        CartPopUpAndItemInfoPage cartPopUpAndItemInfoPage = initElements(driver, CartPopUpAndItemInfoPage.class);

        //select random on template and on cart offers
        cartPopUpAndItemInfoPage.selectRandomOffer(OfferTypes.OnCart);
        cartPopUpAndItemInfoPage.selectRandomOffer(OfferTypes.OnTemplate);

        //get order price from template page
        order.setOrderTotal(cartPopUpAndItemInfoPage.getOrderTotalPrice());

        //go to checkout page
        cartPopUpAndItemInfoPage.clickCheckoutNowButton();
        waitPageLoader("checkout.php");
        waitForTextIsPresent(cartPopUpAndItemInfoPage.orderTotalPrice);

        //Pay By pay pal
        CheckoutPage checkoutPage = initElements(driver, CheckoutPage.class);
        checkoutPage.payBy(PaymentMethods.PayPal);
        waitPageLoader("paypal");

        //check order amount
        PayPalPage payPalPage = initElements(driver, PayPalPage.class);
        assertTrue(getCurrentURL().contains("paypal"), "Incorrect url");
        assertEquals(order.getOrderTotal(), payPalPage.getOrderAmount(), "Incorrect order amount");

    }
}
