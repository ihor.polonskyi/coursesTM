package smoketestsuite;

import businessobjects.User;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import utility.Log;
import utility.services.CookiesService;
import utility.services.ManageUrlService;
import utility.services.WaiterService;
import utility.services.WebElementService;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static utility.services.WebDriverFactory.getDriverInstance;

/**
 * Created by igorp on 19/08/17.
 */
public class BaseTest {

    public WebDriver driver;

    public CookiesService cookiesService;
    public ManageUrlService manageUrlService;
    public WaiterService waiterService;
    public WebElementService webElementService;
    public List<String> cookies;

    public User userEmail = new User("properties/defaultUser.properties");
    public User userFacebook = new User("properties/facebookUser.properties");
    public User userNew = new User("properties/newUser.properties");
    public User user = new User();

    @Parameters({"browser", "node", "cookies"})
    @BeforeTest
    public void runBrowser(@Optional("chrome") String browserValue,
                           @Optional("http://localhost:4444/wd/hub") String nodeValue,
                           @Optional("lsp") String cookies) throws MalformedURLException {

        driver = getDriverInstance(browserValue, nodeValue);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

        getServiceConstructors();
        cookiesToIgnore(cookies);
    }

    @AfterTest
    public void closeBrowser() {
        driver.quit();
        Log.info("Browser closed");
    }

    private void getServiceConstructors(){
        cookiesService = new CookiesService(driver);
        manageUrlService = new ManageUrlService(driver);
        waiterService = new WaiterService(driver);
        webElementService = new WebElementService(driver);
    }

    private void cookiesToIgnore(String cookiesToIgnore){
        cookies = Arrays.asList(cookiesToIgnore.split("\\s*,\\s*"));
    }
}
