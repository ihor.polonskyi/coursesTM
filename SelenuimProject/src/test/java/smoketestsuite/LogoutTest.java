package smoketestsuite;

import org.testng.annotations.AfterMethod;
import pageobjects.SitePages.IndexPage;

import static org.openqa.selenium.support.PageFactory.initElements;
import static utility.services.ReportService.assertTrue;
import static utility.services.WaiterService.waitForElementDisappear;
import static utility.services.WebElementService.elementIsDisplayed;

/**
 * Created by igorp on 22/08/17.
 */
public class LogoutTest extends LoginTest {

    @AfterMethod
    public void logoutTest() {

        //logout from header
        IndexPage indexPage = initElements(driver, IndexPage.class);
        indexPage.logout();

        //check that user avatar disappeared
        waitForElementDisappear(indexPage.userAvatar, "User avatar");
        assertTrue(elementIsDisplayed(indexPage.accountLink, "Account Link"),
                "Account link is not visible");
    }
}
