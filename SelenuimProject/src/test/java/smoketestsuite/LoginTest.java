package smoketestsuite;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.SitePages.IndexPage;
import utility.Constants;
import utility.Log;

import java.io.IOException;

import static org.openqa.selenium.support.PageFactory.initElements;
import static utility.services.CookiesService.getCookieValue;
import static utility.services.FileReaderService.urlDecode;
import static utility.services.ManageUrlService.getUrlWithSettingCookies;
import static utility.services.ReportService.assertEquals;
import static utility.services.ReportService.assertTrue;
import static utility.services.WaiterService.waitCssLoad;
import static utility.services.WaiterService.waitForElementVisible;
import static utility.services.WebElementService.elementIsDisplayed;

/**
 * Created by igorp on 21/08/17.
 */
public class LoginTest extends BaseTest {


    @Test(dataProvider = "loginMethods")
    public void loginEmailTest(String loginMethod) {
        IndexPage indexPage = initElements(driver, IndexPage.class);

        //go to tm.com
        getUrlWithSettingCookies(Constants.URL, cookies);
        waitCssLoad();

        //login and go back to index page
        selectLoginMethod(loginMethod, indexPage);

        //check that user logged in - user avatar and cookie lgn value
        waitForElementVisible(indexPage.userAvatar);
        assertTrue(elementIsDisplayed(indexPage.userAvatar, "User avatar"), "User avatar is not visible");

        assertEquals(urlDecode(getCookieValue("lgn")), user.getEmail(), "Incorrect cookie lgn value");

    }

    @DataProvider(name = "loginMethods")
    public static Object[][] loginMethods() throws IOException {
        return new Object[][]{
                {"Email"},
                {"Facebook"}
        };
    }

    private void selectLoginMethod(String method, IndexPage indexPage) {

        switch (method) {
            case "Email":
                Log.info("Login By Email");
                indexPage.logInWithRedirectToIndexPage(userEmail);
                user.setEmail(userEmail.getEmail());
                waitCssLoad();
                break;
            case "Facebook":
                Log.info("Login By Facebook");
                indexPage.logInWithRedirectToIndexPageByFacebook(userFacebook);
                user.setEmail(userFacebook.getEmail());
                break;
            default:
                Log.info("Incorrect variant to login, so login by Email");
                indexPage.logInWithRedirectToIndexPage(userEmail);
                user.setEmail(userEmail.getEmail());
                waitCssLoad();
        }

    }
}
