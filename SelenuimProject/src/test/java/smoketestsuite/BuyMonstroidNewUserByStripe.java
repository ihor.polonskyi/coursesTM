package smoketestsuite;

import businessobjects.Order;
import org.testng.annotations.Test;
import pageobjects.PaymentsPages.StripePage;
import pageobjects.SitePages.CartPopUpAndItemInfoPage;
import pageobjects.SitePages.CheckoutPage;
import pageobjects.SitePages.IndexPage;
import pageobjects.SitePages.TemplatePage;
import utility.Constants;
import utility.services.Enums.PaymentMethods;

import static org.openqa.selenium.support.PageFactory.initElements;
import static utility.services.ManageUrlService.getCurrentURL;
import static utility.services.ManageUrlService.getUrlWithSettingCookies;
import static utility.services.ReportService.assertEquals;
import static utility.services.ReportService.assertTrue;
import static utility.services.WaiterService.*;

/**
 * Created by igorp on 1/09/17.
 */
public class BuyMonstroidNewUserByStripe extends BaseTest{
    Order order = new Order();

    @Test
    public void buyMonstroidNewUserByStripe() {

        //go to tm.com
        getUrlWithSettingCookies(Constants.URL, cookies);
        waitCssLoad();

        //login by present user
        IndexPage indexPage = initElements(driver, IndexPage.class);

        //get monstroid page
        indexPage.search("55555");

        //click on get it now button
        TemplatePage templatePage = initElements(driver, TemplatePage.class);
        templatePage.clickOnGetItNowButton();
        CartPopUpAndItemInfoPage cartPopUpAndItemInfoPage = initElements(driver, CartPopUpAndItemInfoPage.class);

        //select on template and on cart offers
        cartPopUpAndItemInfoPage.selectOffer("OnCart", Constants.ON_CART_OFFER_NAME);
        waitForElementClickable(cartPopUpAndItemInfoPage.checkoutNowButton);
        cartPopUpAndItemInfoPage.selectOffer("OnTemplate", Constants.ON_TEMPLATE_OFFER_NAME);
        waitForElementClickable(cartPopUpAndItemInfoPage.checkoutNowButton);

        //get order price from template page
        order.setOrderTotal(cartPopUpAndItemInfoPage.getOrderTotalPrice());

        //go to checkout page
        cartPopUpAndItemInfoPage.clickCheckoutNowButton();
        waitPageLoader("checkout.php");
        waitForTextIsPresent(cartPopUpAndItemInfoPage.orderTotalPrice);

        //Insert new user data
        CheckoutPage checkoutPage = initElements(driver, CheckoutPage.class);
        checkoutPage.insertNewUserData(userNew);

        //Pay By Stripe
        checkoutPage.payBy(PaymentMethods.Stripe);
        waitPageLoader("stripe");

        //check order amount
        StripePage stripePage = initElements(driver, StripePage.class);
        assertTrue(getCurrentURL().contains("stripe"), "Incorrect url");
        assertEquals(order.getOrderTotal(), stripePage.getOrderAmount(), "Incorrect order amount");
    }
}
